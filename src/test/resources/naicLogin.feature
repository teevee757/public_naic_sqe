Feature: NAIC Invalid Login

  Scenario: Invalid username and password
    Given NAIC Login Portal
    When User entered invalid username "InvalidUser" and password "InvalidPassword"
    Then Error message "Login failed, please try again."