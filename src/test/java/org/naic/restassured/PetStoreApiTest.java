package org.naic.restassured;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.List;

import static io.restassured.RestAssured.get;
import static io.restassured.RestAssured.given;

public class PetStoreApiTest {
    private static String petStore = "https://petstore.swagger.io";
    private static String availablePet = "/v2/pet/findByStatus?status=available";

    @Test
    public void petStore200Status() {
        given().when().get(petStore).then().statusCode(200);
    }

    @Test
    public void testAvailablePetSize() {
        List<String> pets = get(petStore.concat(availablePet))
                .path("");
        Assert.assertTrue(pets.size() > 1);
    }
}
