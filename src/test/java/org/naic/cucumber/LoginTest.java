package org.naic.cucumber;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginTest {
    private static WebDriver webDriver;
    private static String naicLoginPortal = "https://eapps.naic.org/lhub";
    private static String loginButtonId = "cui-login";
    private static String usernameId = "INT_USERNAME";
    private static String passwordId = "INT_PASSWORD";
    private static String loginSubmitButtonId = "INT_LOGIN_BTN";
    private static String messageId = "INT_MESSAGE_WRAPPER";

    @Before
    public void setUp() throws Exception {
        WebDriverManager.chromedriver().setup();

        ChromeOptions option = new ChromeOptions();
        option.addArguments( "--headless" );
        option.addArguments( "--ignore-certificate-errors" );
        option.addArguments( "--no-sandbox" );
        option.addArguments( "--disable-extensions" );
        option.addArguments( "--disable-dev-shm-usage" );
        option.setCapability( ChromeOptions.CAPABILITY, option );

        webDriver = new ChromeDriver();
    }

    @After
    public void tearDown() throws Exception {
        webDriver.quit();
    }

    @Given( "NAIC Login Portal" )
    public void naicLoginPortal() {
        webDriver.get( naicLoginPortal );

        WebDriverWait wait = new WebDriverWait( webDriver, 40 );
        wait.until(ExpectedConditions.elementToBeClickable( By.id( loginButtonId ) ) );
        webDriver.findElement( By.id( loginButtonId ) ).click();
    }

    @When( "User entered invalid username {string} and password {string}" )
    public void invalidUsernameAndPassword( String username, String password ) {
        webDriver.findElement( By.id( usernameId ) ).sendKeys( username );
        webDriver.findElement( By.id( passwordId ) ).sendKeys( password );
        webDriver.findElement( By.id( loginSubmitButtonId ) ).submit();
    }

    @Then("Error message {string}")
    public void errorMessage( String message ){
        WebElement element = webDriver.findElement( By.id( messageId ) );
        Assert.assertEquals( message,element.getText() );
    }
}
